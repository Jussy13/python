import re

text = "Что такое регулярные выражения и как их использовать? Говоря простым языком, регулярное выражение — это " \
       "последовательность символов, используемая для поиска и замены текста в строке или файле. Как уже было " \
       "упомянуто, их поддерживает множество языков общего назначения: Python, Perl, R. Так что изучение " \
       "регулярных выражений рано или поздно пригодится. "

pattern = "регулярн[а-яёА-ЯЁ]*\s+выражен[а-яёА-ЯЁ]*"
text_new = re.sub(pattern, "RegExp", text)
print(text_new)
pattern = re.compile(pattern)
text_new = pattern.sub("RegExp", text)
print(text_new)
result = re.match("Что", text)
print(result)
print(result.group(0))
result = pattern.search(text)
print(result.group(0))
result = pattern.findall(text)
print(result)
words = re.split("[\s,!?.-]", text)
print(words)
print('test')
